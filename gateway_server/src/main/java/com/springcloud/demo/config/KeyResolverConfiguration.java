package com.springcloud.demo.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author Shubo Dai
 * @description KeyResolverConfiguration配置
 */
@Configuration
public class KeyResolverConfiguration {

    /**
     * 基于路径的限流规则
     */
//    @Bean
//    public KeyResolver pathKeyResolver(){
//        //自定义KeyResolver
//        return new KeyResolver() {
//            /**
//             * @param exchange 上下文参数。
//             */
//            @Override
//            public Mono<String> resolve(ServerWebExchange exchange) {
//                return Mono.just(exchange.getRequest().getPath().toString());
//            }
//        };
//    }
//
//    /**
//     * 基于ip地址的限流
//     */
//    @Bean
//    public KeyResolver ipKeyResolver(){
//        return exchange -> Mono.just(
//          exchange.getRequest().getHeaders().getFirst("X-Forwarded-For")
//        );
//    }
//
//    /**
//     * 基于参数的限流
//     */
//    @Bean
//    public KeyResolver userKeyResolver(){
//        return exchange -> Mono.just(
//            exchange.getRequest().getQueryParams().getFirst("user")
//        );
//    }
}
