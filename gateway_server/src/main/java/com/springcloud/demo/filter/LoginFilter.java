package com.springcloud.demo.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author Shubo Dai
 * @description 过滤器
 */
//@Component
public class LoginFilter implements GlobalFilter, Ordered {

    /**
     * 真正执行逻辑的代码
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("进入过滤器");
        String token = exchange.getRequest().getQueryParams().getFirst("access-token");
        if(token == null){
            System.out.println("没有登陆！");
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            return exchange.getResponse().setComplete();
        }
        return chain.filter(exchange);
    }

    /**
     * 过滤器链：返回值越小，越靠前执行。
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
