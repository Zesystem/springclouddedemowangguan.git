package com.springcloud.demo.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName LoginFilter
 * @Description
 * @Author 戴书博
 * @Date 2020/5/28 16:24
 * @Version 1.0
 **/
@Component
public class LoginFilter extends ZuulFilter {

    /**
     * 类型：
     *      pre
     *      routing
     *      post
     *      error
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * 指定过滤器的执行顺序
     * 返回值越小，执行顺序越靠前
     */
    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * 当前过滤器是否生效
     * true：使用此过滤器
     * false：不使用此过滤器
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 执行过滤器的方法
     */
    @Override
    public Object run() throws ZuulException {
        System.out.println("================进入过滤器=================");
        //获取上下文对象
        RequestContext ctx = RequestContext.getCurrentContext();
        //获取request对象
        HttpServletRequest req = ctx.getRequest();
        //从请求中获取token
        String token = req.getParameter("access-token");
        //判断
        if(token == null || token.equals("")){
            // 没有token，登录校验失败，拦截
            ctx.setSendZuulResponse(false);
            // 返回401状态码。也可以考虑重定向到登录页。
            ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
        }
        // 校验通过，可以考虑把用户信息放入上下文，继续向后执行
        return null;
    }
}
