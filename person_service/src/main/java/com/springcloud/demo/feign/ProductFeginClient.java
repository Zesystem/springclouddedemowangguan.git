package com.springcloud.demo.feign;

import com.springcloud.demo.entity.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @ClassName ProductFeginClient
 * @Description
 * @Author Zesysterm
 * @Date 2020/5/26 11:30
 * @Version 1.0
 **/
@FeignClient(name = "service-product")
public interface ProductFeginClient {

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public Product findById(@PathVariable("id") Long id);
}
