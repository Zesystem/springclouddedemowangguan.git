package com.springcloud.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author Shubo Dai
 * @description 提供者服务类，@EnableEurekaClient、@EnableDiscoveryClient标识激活eureka客户端
 */
@SpringBootApplication
@EntityScan("com.springcloud.demo.entity")
@EnableEurekaClient
public class ProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class,args);
	}
}
